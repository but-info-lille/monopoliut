Barbeau Simon
Debusschere Sacha                         MONOPOL'I.U.T.
Hessel Aymeric
Ameloot Antoine 

# Sprint 5

## Démo + Planification du sprint suivant

### Ce que nous avons fait durant ce sprint
-Niveau et loyer des propriétés
-Amélioration au passage du propriétaire

### Ce que nous allons faire durant le prochain sprint
-Lancement du jeu
-Creation joueur
-Sortir après trois tours
-Gestion des joueurs
-Perdu !

## Rétrospective

### Sur quoi avons nous butté ?
Encore des problèmes avec git
Le code qui se complique et la présence de messages d'erreurs 
Une personne de notre groupe travaillait sur une branche diffèrentes donc il a oublié de faire des pull et au moment ou il push, deux versions diffèrentes se sont créé

### PDCA
* De tous ces éléments quel est celui que vous voulez améliorer ?
Le problèmes avec git

* Comment pouvez-vous mesurer qu'il s'améliore ?
Ne plus avoir de messages d'erreurs 

* Quelles sont toutes les options possible pour l'améliorer ?
Création d'une branche altérnative 
Augmenter la fréquence des pull
Ne pas travailler sur les mêmes fichiers

* Qu'allez-vous tester pour l'améliorer ?
Augmenter la fréquence des pull

# Mémo
N’oubliez pas d’ajouter une photo du radiateur d’information au moment de la rétrospective