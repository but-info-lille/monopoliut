Barbeau Simon
Debusschere Sacha                         MONOPOL'I.U.T.
Hessel Aymeric
Ameloot Antoine 

# Sprint 7

## Démo + Planification du sprint suivant

### Ce que nous avons fait durant ce sprint
-Perdu
-Gestion des joueurs

### Ce que nous allons faire durant le prochain sprint
-Lancement du jeu
-Interaction joueur
-Carte chance
-Interface utilisateur

## Rétrospective

### Sur quoi avons nous butté ?
Lister ici tout ce qui était un peut moins bien que parfait.
* Avez-vous terminé tout ce que vous vous étiez engagé à faire ?
* Étiez -vous prêts au moment de la démo ?
* ...

### PDCA
* De tous ces éléments quel est celui que vous voulez améliorer ?
* Comment pouvez-vous mesurer qu'il s'améliore ?
* Quelles sont toutes les options possible pour l'améliorer ?
* Qu'allez-vous tester pour l'améliorer ?

# Mémo
N’oubliez pas d’ajouter une photo du radiateur d’information au moment de la rétrospective.