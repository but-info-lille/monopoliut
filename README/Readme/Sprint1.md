Barbeau Simon
Debusschere Sacha                         MONOPOL'I.U.T.
Hessel Aymeric
Ameloot Antoine                

# Sprint 1

## Démo + Planification du sprint suivant

### Ce que nous avons fait durant ce sprint
Ce que l'on voulait faire : 
-Joueur
-Achat
-afficher propriétés possédées 

Rien n'a été fait durant le sprint 1 car aucune démo n'a été faite

### Ce que nous allons faire durant le prochain sprint
(-Achat) presque terminée
(-Joueur) presque terminée
(-afficher propriétés possédées) presque terminée 
-plateau de jeu

## Rétrospective

### Sur quoi avons nous butté ?
Nous avons eu des problèmes avec git, donc au niveau de la démo nous étions pas prêt.


### PDCA
* De tous ces éléments quel est celui que vous voulez améliorer ?
-La démo

* Comment pouvez-vous mesurer qu'il s'améliore ?
-Par rapport au nombre d'éléments présent dans la démo 

* Quelles sont toutes les options possible pour l'améliorer ?
-Accorder plus de temps 

* Qu'allez-vous tester pour l'améliorer ?
-Etre plusieurs à s'en occuper 


# Mémo
N’oubliez pas d’ajouter une photo du radiateur d’information au moment de la rétrospective.