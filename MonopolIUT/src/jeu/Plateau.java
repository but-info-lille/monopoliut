package jeu;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Plateau {
	
	//attributes

	private Case[] plateau;
	private Joueur[] tabJoueurs;
	
	
	//constructor
	
	public Plateau(Case[] plateau, Joueur[] tabJoueurs){
		this.plateau=plateau;
		this.tabJoueurs=tabJoueurs;
	}
	
	public Plateau(Joueur[] tabJoueurs){
		this(new Case[Constantes.TAILLE_PLATEAU],tabJoueurs);
	}
	
	public Plateau(Case[] plateau){
		this(plateau,new Joueur[Constantes.NB_JOUEUR]);
	}
	
	public Plateau() {
		this(new Case[40],new Joueur[5]);
	}
	
	
	//getters & setters
	
	public Case[] getPlateau() {
		return plateau;
	}
	
	public void setPlateau(Case[] plateau) {
		this.plateau = plateau;
	}

	public Joueur[] getTabJoueurs() {
		return tabJoueurs;
	}

	public void setTabJoueurs(Joueur[] tabJoueurs) {
		this.tabJoueurs = tabJoueurs;
	}


	//methods
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plateau other = (Plateau) obj;
		if (!Arrays.equals(plateau, other.plateau))
			return false;
		if (!Arrays.equals(tabJoueurs, other.tabJoueurs))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Plateau [plateau=" + Arrays.toString(plateau) + ", tabJoueurs=" + Arrays.toString(tabJoueurs) + "]";
	}
	
	public void affichage() {
		for (Case c:plateau) {
			System.out.print(c.toString());
			for(Joueur j: this.getTabJoueurs()) {
				if (c.getIdCase() == j.getPosition()) {
					System.out.print("   "+ j);
				}
			}
			System.out.println();
		}
	}
	
}
