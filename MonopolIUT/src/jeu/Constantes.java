package jeu;

public final class Constantes {
	
	public final static double MAISON2=1.2;
	public final static double MAISON3=1.5;
	public final static double IMMEUBLE=2.0;
	

	public final static int ARGENT_PAR_TOUR = 200; 
	public final static int TAILLE_PLATEAU = 40;
	public final static int NB_JOUEUR = 1;
	
	public final static String RESSOURCES_PATH = System.getProperty("user.dir") + System.getProperty("file.separator") + "ressources" + System.getProperty("file.separator");
	public final static String CSV_PROPRIETES = RESSOURCES_PATH + "proprietes.csv";
	public final static String CSV_EVENEMENTS =  RESSOURCES_PATH + "evenements.csv";
	public final static String TITRE =  RESSOURCES_PATH + "titre.txt";
	public final static String PLATEAU =  RESSOURCES_PATH + "plateau.txt";
	
	public final static String RESSOURCES_PATH_CASE = System.getProperty("user.dir") + System.getProperty("file.separator") + "ressources" + System.getProperty("file.separator")+ "cases" + System.getProperty("file.separator");
	public final static String CASE = RESSOURCES_PATH_CASE + "case";
	
	public final static String RESSOURCES_PATH_DE = System.getProperty("user.dir") + System.getProperty("file.separator") + "ressources" + System.getProperty("file.separator")+ "des" + System.getProperty("file.separator");
	public final static String DE = RESSOURCES_PATH_DE + "de";

	public final static String RESSOURCES_PATH_PLATEAU = System.getProperty("user.dir") + System.getProperty("file.separator") + "ressources" + System.getProperty("file.separator")+ "plateauxJoueurs" + System.getProperty("file.separator");
	public final static String PLATEAUJOUEUR = RESSOURCES_PATH_PLATEAU + "plateau";

	
	public final static int ARGENTDEPART = 1500;
}
