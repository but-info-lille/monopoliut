package jeu;

import java.util.ArrayList;
import java.util.Random;

public class Joueur {

	//attributes

	private long  idJoueur;
	private int position;
	private String nom;
	private ArrayList<Propriete> proprietes;
	private int argentPossede;
	private int score=0;
	private boolean estEnJeu=true;
	private boolean estEnPrison = false;
	private int compteurPrison;


	//constructor

	public Joueur(long idJoueur, int position, String nom, ArrayList<Propriete> proprietes, int argentPossede) {
		this.idJoueur=idJoueur;
		this.position=position;
		this.nom=nom;
		this.proprietes=proprietes;
		this.argentPossede=argentPossede;
	}

	public Joueur(long idJoueur, String nom) {
		this(idJoueur, 0, nom, new ArrayList<Propriete>(), Constantes.ARGENTDEPART);
	}


	//getters & setters



	public long getIdJoueur() {
		return idJoueur;
	}

	public void setIdJoueur(long idJoueur) {
		this.idJoueur = idJoueur;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position, Plateau p) {
		this.position = position;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getArgentPossede() {
		return argentPossede;
	}

	public void setArgentPossede(int argentPossede) {
		this.argentPossede = argentPossede;
	}	

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public ArrayList<Propriete> getProprietes() {
		return proprietes;
	}

	public boolean estProprio(Propriete p) {
		if (p.getProprio()!=null) {
			if (p.getProprio().equals(this)) {
				return true;
			}
		}
		return false;
	}


	public boolean isEstEnJeu() {
		return estEnJeu;
	}

	public void setEstEnJeu(boolean estEnJeu) {
		this.estEnJeu = estEnJeu;
	}

	public boolean isEstEnPrison() {
		return estEnPrison;
	}

	public void setEstEnPrison(boolean estEnPrison) {
		this.estEnPrison = estEnPrison;
	}

	//methods

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Joueur other = (Joueur) obj;
		if (argentPossede != other.argentPossede)
			return false;
		if (idJoueur != other.idJoueur)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (position != other.position)
			return false;
		if (proprietes == null) {
			if (other.proprietes != null)
				return false;
		} else if (!proprietes.equals(other.proprietes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		System.out.println("-----------------------");
		return "     Nom : " + this.getNom() + "\n" +  "     Argent : " + this.getArgentPossede() + "\n" + "     Position : " + this.getPosition() + "\n" + "-----------------------"; 
	}

	public void perdu() {
		this.estEnJeu=false;
	}

	//Joueur j1 = new Joueur(1, 0, "J1", 0);
	public int[] lancerDe() {
		Random de1 = new Random();
		int[] tabVal = new int[2];
		tabVal[0] = de1.nextInt(6)+1;
		tabVal[1] = de1.nextInt(6)+1;
		return tabVal;
	}

	public void move(int compteur, Plateau p) {
		int[] newPos = lancerDe();
		int posSuivant = this.getPosition() + newPos[0]+ newPos[1];
		if (posSuivant >= 39) {
			this.resetTour(posSuivant-39, p);
		}else {
			this.setPosition(posSuivant, p);
			if(this.getPosition() == 30) {
				this.setPosition(10, p);
				estEnPrison = true;
			}
		}
		AffichageAscii.AffichePlateauJoueur(this);
		System.out.println("Vous avancez de " + posSuivant);
		AffichageAscii.AfficheDe(newPos[0]);
		AffichageAscii.AfficheDe(newPos[1]);
		if (newPos[0] == newPos[1]) {
			compteur++;
			System.out.println(this.getNom()+": Vous avez fait "+compteur+" double");
			if (compteur < 3) {
				System.out.println(this.getNom()+": Vous rejouez.");
				move(compteur, p);
			}else {
				this.departPrison(p);
				System.out.println(this.getNom()+": Vous partez en Prison pour exces de chance.");
			}
		}
	}

	public void resetTour(int rest, Plateau p) {
		int newPos = 0+ rest;
		this.setPosition(newPos, p);
		this.setArgentPossede(this.getArgentPossede()+Constantes.ARGENT_PAR_TOUR);
	}

	public boolean departPrison(Plateau p) {
		System.out.println(this.getNom()+": Vous partez en prison.");
		this.setPosition(10, p);
		this.setEstEnPrison(true);
		this.compteurPrison = 3;
		return isEstEnPrison();
	}

	public void sortirPrison() {
		while (this.compteurPrison > 0) {
			if(Utils.question(this.getNom()+": Lancer les des pour essayer de sortir de prison ?")) {
				int[] result = lancerDe();
				System.out.println(this.getNom()+": Vous avez fait "+result[0]+" et "+result[1]);
				if (result[0] == result[1]) {
					this.setEstEnPrison(false);
					System.out.println(this.getNom()+": Vous sortez de prison.");
				}else {
					System.out.println(this.getNom()+": Vous n'avez pas fait un double, vous restez en prison.");
					this.compteurPrison = this.compteurPrison - 1;
				}
			}
			if(this.isEstEnPrison() && Utils.question(this.getNom()+": Voulez-vous payer 50 pour sortir ?")) {
				this.setEstEnPrison(false);
			}
			this.compteurPrison = this.compteurPrison - 1;
		}
		estEnPrison = false;
	}



}
