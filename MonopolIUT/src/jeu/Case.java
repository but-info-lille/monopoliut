package jeu;

import java.util.List;
import java.util.Objects;

public class Case {
	private long idCase;
	private Evenement event;
	private Joueur[] joueurInCase = new Joueur[5];

	public Case(long idCase, Evenement e) {
		super();
		this.idCase = idCase;
		this.event = e;
	}

	public Case(long idCase) {
		this(idCase, null);
	}

	public static Case[] creationPlateau(List<Propriete> prop, List<Evenement> event) {
		Case[] jeu = new Case[Constantes.TAILLE_PLATEAU];
		int c = 0;
		for (int i = 0; i < Constantes.TAILLE_PLATEAU; i++) {
			if (prop.get(c).getIdCase() == i) {
				jeu[i] = prop.get(c);
				c++;
			}
		}
		c = 0;
		for (int i =0; i<Constantes.TAILLE_PLATEAU; i++) {
			if(c < event.size()) {
				if (event.get(c).getIdCase() == i) {
					jeu[i] = event.get(c);
					c++;
				}
			}
		}


		return jeu;
	}

	public boolean isPropriete() {
		return this instanceof Propriete;
	}

	public long getIdCase() {
		return idCase;
	}



	public Joueur[] getJoueurInCase() {
		return joueurInCase;
	}

	public void addJoueurInCase(Joueur joueur) {
		boolean add = false;
		for (int i = 0; i < this.joueurInCase.length; i++) {
			if (! add && this.joueurInCase[i] == null) {
				add = true;
				this.joueurInCase[i] = joueur;
			}
		}
	}

	@Override
	public String toString() {
		AffichageAscii.AfficheCase(this);
		return "";
	}

	@Override
	public int hashCode() {
		return Objects.hash(idCase);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Case other = (Case) obj;
		return idCase == other.idCase;
	}




}