package jeu;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class AffichageAscii {
	
	
	public static void AfficheTitre() {
		System.out.flush();
      try(BufferedReader br = new BufferedReader(new FileReader(Constantes.TITRE))){
      	String st;
  	    while ((st = br.readLine()) != null) {
  	    	System.out.println(st);
  	    }
      }catch(FileNotFoundException e1) {
      	System.err.println(e1.getMessage());
      }catch(IOException e2) {
      	System.err.println(e2.getMessage());
      }
	}
	
	
	public static void AffichePlateauVide() {
		System.out.flush();
      try(BufferedReader br = new BufferedReader(new FileReader(Constantes.PLATEAU))){
      	String st;
  	    while ((st = br.readLine()) != null) {
  	    	System.out.println(st);
  	    }
      }catch(FileNotFoundException e1) {
      	System.err.println(e1.getMessage());
      }catch(IOException e2) {
      	System.err.println(e2.getMessage());
      }
	}
	
	public static void AffichePlateauJoueur(Joueur j) {
		System.out.flush();
      try(BufferedReader br = new BufferedReader(new FileReader(Constantes.PLATEAUJOUEUR+j.getPosition()+".txt"))){
      	String st;
  	    while ((st = br.readLine()) != null) {
  	    	System.out.println(st);
  	    }
      }catch(FileNotFoundException e1) {
      	System.err.println(e1.getMessage());
      }catch(IOException e2) {
      	System.err.println(e2.getMessage());
      }
	}
	
	
	public static void AfficheCase(Case p) {
		System.out.flush();
      try(BufferedReader br = new BufferedReader(new FileReader(Constantes.CASE+p.getIdCase()+".txt"))){
      	String st;
  	    while ((st = br.readLine()) != null) {
  	    	System.out.println(st);
  	    }
      }catch(FileNotFoundException e1) {
      	System.err.println(e1.getMessage());
      }catch(IOException e2) {
      	System.err.println(e2.getMessage());
      }
	}
	
	public static void AfficheDe(int numeroDe) {
		System.out.flush();
      try(BufferedReader br = new BufferedReader(new FileReader(Constantes.DE+numeroDe+".txt"))){
      	String st;
  	    while ((st = br.readLine()) != null) {
  	    	System.out.println(st);
  	    }
      }catch(FileNotFoundException e1) {
      	System.err.println(e1.getMessage());
      }catch(IOException e2) {
      	System.err.println(e2.getMessage());
      }
	}
	
}
