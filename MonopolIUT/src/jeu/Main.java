package jeu;

import java.util.ArrayList;
import java.util.List;


public class Main {

	public static long idJoueur=0;


	public static Joueur[] creerJoueurs(int nbJoueurs) {
		Joueur[] res = new Joueur[nbJoueurs];
		for (int i=0; i<nbJoueurs; i++) res[i] = creationJoueur();
		return res;
	}


	public static Joueur creationJoueur() {
		long id=Main.idJoueur++;
		String nom =Utils.joueur();
		return new Joueur(id,nom);

	}

	public static void main(String[] args) {
		Case[] cases = Case.creationPlateau(Propriete.importationProprietes(), Evenement.importationEvenements());
		int nbJoueur = Utils.questionNombre("Nombre de joueurs", 5);
		Joueur[] joueurs = creerJoueurs(nbJoueur);

		Plateau jeu = new Plateau(cases, joueurs);
		AffichageAscii.AffichePlateauVide();



		boolean jeuEnCour = true;
		while (jeuEnCour) {
			for (Joueur j: joueurs) {
				if(j.isEstEnJeu()) {
					System.out.println(j.toString());
					System.out.println("\n\n\n");
					System.out.println("1 Deplacement");
					System.out.println("2 Interaction");
					int choix = Utils.questionNombre(j.getNom()+": Que voulez-vous faire ?", 2);
					while (j.getArgentPossede()<= 0) {
						if (j.getProprietes().size() > 0) {
							boolean veutVendre = Utils.question(j.getNom()+": voulez-vous vendre une de vos propriete pour continuer de jouer");
							if (veutVendre) {
								for (Propriete p: j.getProprietes()) {
									p.vendrePropriete(j);							
								}
							}else {
								j.setEstEnJeu(false);
							}
						}else {
							j.setEstEnJeu(false);
						}
					}
					switch(choix) {
					case 1:
						move(jeu, j);
						break;
					case 2:
						echange(jeu, j);
						move(jeu, j);
						break;
					}

				}
			}
			int joueurEnJeu = 0;
			for(Joueur j: jeu.getTabJoueurs()) {
				if (j.isEstEnJeu()) {
					joueurEnJeu++;
				}
			}
			if(joueurEnJeu <= 1) {
				jeuEnCour = false;
				Joueur gagnant = null;
				for (Joueur j: jeu.getTabJoueurs()) {
					if (j.isEstEnJeu()) {
						gagnant = j;
					}
				}
				System.out.println(gagnant.getNom()+": BRAVO a "+gagnant.getNom()+" pour avoir gagner cette partie");
			}
		}
	}

	private static void move(Plateau jeu, Joueur j) {
		if(j.isEstEnPrison()) {
			j.sortirPrison();
		}else {
			Utils.question(j.getNom()+": Lancer les des ?");
			j.move(0, jeu);
			System.out.println("\n\n\n\n\n");
			AffichageAscii.AfficheCase(jeu.getPlateau()[j.getPosition()]);
			if (jeu.getPlateau()[j.getPosition()].isPropriete()) {
				Propriete p = (Propriete)jeu.getPlateau()[j.getPosition()];
				if (p.getProprio() == null) {
					boolean rep = Utils.question(j.getNom()+": Voulez vous acheter cette proprietee ?");
					if(rep) {
						p.acheter(j);
					}
				}else if(! p.getProprio().equals(j)) {
					p.payer(p.getLoyer(), p.getProprio(), j);
				}else {
					p.ameliorer(j);

				}
			}else {
				Evenement.modifAppliquee(j, jeu);
			}
		}
	}

	private static void echange(Plateau jeu, Joueur j) {
		List<Propriete> props = new ArrayList<Propriete>();
		int argent;
		boolean fini;
		int compteur = 1;
		for(Joueur joueur: jeu.getTabJoueurs()) {
			if (! joueur.equals(j)) {
				System.out.println(compteur+": "+joueur.getNom());
				compteur++;
			}
		}
		int rep = Utils.questionNombre(j.getNom()+": Avec qui voulez vous echanger ?", jeu.getTabJoueurs().length-1);
		Joueur joueurEchange = jeu.getTabJoueurs()[rep];
		do {
			for (Propriete p: joueurEchange.getProprietes()) {
				System.out.println(p);
			}
			System.out.println(j.getNom()+": veuillez choisir les propriete a echanger dans la liste ci dessus (par id).");
			System.out.println(j.getNom()+": tapez 40 lorsque vous avez fini.");
			int temp;
			do {
				temp = Utils.questionNombre("", 40);
				if(temp != 40) {
					Propriete prop = (Propriete)jeu.getPlateau()[temp];
					if (prop.getProprio().equals(joueurEchange)) {
						props.add(prop);
					}
				}
			}while(temp != 40);
			argent = Utils.inputNombre(j.getNom()+": combien d'argent vouler vous mettre ?(un nombre negatif vous fait gagner de l'argent)");
			fini = Utils.question(j.getNom()+": valider ?");
		}while(! fini);
		if(argent > 0) {
			System.out.println("argent donnee :"+argent);
		}else {
			System.out.println("argent recu :"+argent);
		}
		System.out.println("propriete donnee: ");
		for (Propriete pr: props) {
			System.out.println(pr);
		}
		boolean transactionFin = Utils.question(joueurEchange+": ete vous d'accord ?");
		if(transactionFin) {
			for (Propriete p: props) {
				joueurEchange.getProprietes().remove(p);
				j.getProprietes().add(p);
				p.setProprio(j);
			}
			j.setArgentPossede(j.getArgentPossede()-argent);
			joueurEchange.setArgentPossede(joueurEchange.getArgentPossede()+argent);
		}
	}

}
