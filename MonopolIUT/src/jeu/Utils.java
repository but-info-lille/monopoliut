package jeu;

import java.util.Scanner;

public class Utils {

	private static Scanner reponse = new Scanner(System.in);

	public static boolean question(String quest) {
		String s;
		do {
			System.out.println(quest+" y/n");
			s=reponse.next();
		}while(!s.equals("y")&&!s.equals("n"));
		if (s.equals("y")) {

			return true;
		}
		return false;
	}

	public static String joueur() {
		System.out.println("Quel est votre nom?");
		String s= reponse.next();
		return s;
	}

	public static int questionNombre(String question, int max) {
		String rep = "";
		char temp;
		
		do {
			System.out.println(question);
			rep = reponse.next();
			temp = rep.charAt(0);
		}while(temp <= 48 && temp >= 48+max);
		return Integer.valueOf(rep);
	}


	public static int inputNombre(String question) {
		int rep = 0;
		
		try {
			System.out.println(question);
			rep = reponse.nextInt();
		}catch(Exception e1) {
			System.out.println(e1);
		}
		return rep;
	}



}
