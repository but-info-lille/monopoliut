package jeu;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Evenement extends Case{

	private int argent, score, position;
	private String description;
	
	
	public Evenement(long id, int argent, int score, int position, String desciption) {
		super(id);
		this.argent = argent;
		this.score = score;
		this.position = position;
		this.description = desciption;
	}
	
	public void appliquer(Joueur j, Plateau p) {
		j.setArgentPossede(j.getArgentPossede()+this.getArgent());
		if (this.getPosition() != -1) {
			j.setPosition(this.getPosition(), p);
		}
		j.setScore(j.getScore()+this.getScore());
	}



	public int getArgent() {
		return argent;
	}


	public int getScore() {
		return score;
	}


	public int getPosition() {
		return position;
	}


	public String getDesciption() {
		return description;
	}
	
	@Override
	public String toString() {
		String res = description + " [";
		if (argent != 0) {
			res += "argent: " + argent;
		} else if (position != 0) {
			res += "position: " + position;
		} else if (score != 0) {
			res += "score: " + score;
		}
		return res + "]";
	}
	
	private static Evenement load(String toLoad) throws Exception {
		String[] datas = toLoad.split(";");
		if (datas.length == 5) {
			try {
				int id = Integer.valueOf(datas[0]);
				int argent = Integer.valueOf(datas[2]);
				int score = Integer.valueOf(datas[3]);
				int position = Integer.valueOf(datas[4]);
				String desc = datas[1];
				Evenement e = new Evenement(id, argent, score, position, desc);
				return e;
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		
		} else {
			throw new Exception("Fichier CSV corrompu"); 
		}
		return null;
	}
	
	public static ArrayList<Evenement> importationEvenements() {
		ArrayList<Evenement> res = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(Constantes.CSV_EVENEMENTS))) {
			String line = br.readLine();
		    while (line != null) {
		    	Evenement e = Evenement.load(line);
		    	res.add(e);
		    	line = br.readLine();
		    }
		} catch (IOException e) {
			System.err.println(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public static void modifAppliquee(Joueur j, Plateau p) {
		//this.importationEvenements();
		if (j.getPosition() == 2 || j.getPosition() == 7 || j.getPosition() == 8 || j.getPosition() == 17 || j.getPosition() == 22 || j.getPosition() == 33 || j.getPosition() == 36) {
			Chance.event(j, p);
		}
		else if (j.getPosition() == 30) {
			j.departPrison(p);
		}
		else if(j.getPosition() == 4) {
			System.out.println(j.getNom()+": C'est l'heure de payer les impots ! Vous donnez 100$.");
			j.setArgentPossede(j.getArgentPossede() - 100);
		}
		else if (j.getPosition() == 38) {
			System.out.println(j.getNom()+": C'est l'heure de payer les impots mais l'Etat manque d'argent, c'est donc plus chere ! Vous donnez donc 150$.");
			j.setArgentPossede(j.getArgentPossede() - 150);
		}
		else if(j.getPosition() == 12 || j.getPosition() == 28) {
			j.setArgentPossede(j.getArgentPossede() - 50);
		}
	}
}
