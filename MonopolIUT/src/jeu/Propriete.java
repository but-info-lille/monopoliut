package jeu;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Propriete extends Case{
	
	private String nom;
	private int prixAchat;
	private int loyer;
	private int niveau;
	private Joueur proprio;
	private final static int prixAmelioration= 200;
	
	
	public Propriete(long idCase, String nom, int prixAchat, Evenement e) {
		super(idCase, e);
		this.nom = nom;
		this.prixAchat = prixAchat;
		this.loyer=this.prixAchat/2;
		this.niveau = 0;
		this.proprio = null;
	}
	
	


	/*public Propriete(long idCase, Evenement e, String nom, int prixAchat) {
		this(idCase, nom, new HashMap<>(), e);
	}*/


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public int getPrixAchat() {
		return prixAchat;
	}


	public void setPrixAchat(int prixAchat) {
		this.prixAchat = prixAchat;
	}


	public int getLoyer() {
		return loyer;
	}
	
	


	public void setLoyer(int loyer) {
		this.loyer = loyer;
	}




	public int getNiveau() {
		return niveau;
	}


	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}


	public Joueur getProprio() {
		return proprio;
	}


	public void setProprio(Joueur proprio) {
		this.proprio = proprio;
	}
	
	


	public int getPrixAmelioration() {
		return prixAmelioration;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(loyer, nom, prixAchat, proprio);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Propriete other = (Propriete) obj;
		return Objects.equals(loyer, other.loyer) && Objects.equals(nom, other.nom) && prixAchat == other.prixAchat
				&& Objects.equals(proprio, other.proprio);
	}
	

	public boolean acheter(Joueur j) {
		if (this.getProprio()==null) {
			this.setProprio(j);
			this.setNiveau(1);
			j.setArgentPossede(j.getArgentPossede() - this.getPrixAchat());
			j.getProprietes().add(this);
			return true;
		}
		return false;
	}

	public void payer(int valeur, Joueur j, Joueur autreJoueur) {
		if (!this.peutPayer(valeur, j)) {
			this.vendrePropriete(j);
		}
		j.setArgentPossede(j.getArgentPossede() - valeur);
		autreJoueur.setArgentPossede(autreJoueur.getArgentPossede() + valeur);

	}

	public void vendrePropriete(Joueur j) {
		if (j.getProprietes().size() >0 &&  this.veutVendre(j)) {
			int cpt=0;
			for (Propriete p:j.getProprietes()) {
				cpt++;
				System.out.println(cpt+"  -  "+p.toString());
			}
			Scanner scan = new Scanner(System.in);
			int reponse;
			do {
				System.out.println(j.getNom()+": quelle propriete voulez vous vendre?");
				reponse = Integer.valueOf(scan.nextInt()+"");
			}while (reponse >cpt);
			Propriete propri = j.getProprietes().get(reponse-1);
			if (propri.getNiveau()==1) {
				propri.setProprio(null);
				propri.setNiveau(0);
				j.setArgentPossede(j.getArgentPossede()+(int)(propri.getPrixAchat()*0.75));
				j.getProprietes().remove(reponse-1);
			}else {
				j.setArgentPossede(j.getArgentPossede()+(int)(propri.getPrixAmelioration()*0.75));
				propri.setNiveau(propri.getNiveau()-1);
			}
			scan.close();
		}else {
			j.perdu();
		}

	}

	public boolean peutPayer(int valeur, Joueur j) {
		return j.getArgentPossede()>=valeur;
	}

	public boolean ameliorer(Joueur j) {
		if (j.estProprio(this)) {

			if(this.getNiveau()<4) {
				if(this.veutAmeliorer(j)) {
					if(j.getArgentPossede()>=this.prixAmelioration()) {
						j.setArgentPossede(j.getArgentPossede()-(int)this.prixAmelioration());
						this.setNiveau(this.getNiveau()+1);
						this.setLoyer((int)(this.getPrixAchat()/2*(this.getNiveau()*1.5)));
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean veutAmeliorer(Joueur j) {
		if (Utils.question(j.getNom()+": Voulez-vous ameliorer votre propriete (pour 200$)?")) {
			return true;
		}
		return false;
	}

	public boolean veutVendre(Joueur j) {
		if(Utils.question(j.getNom()+": Voulez vous vendre une propriete?")) {
			return true;
		}
		return false;
	}
	
	public double prixAmelioration() {
		if (this.getNiveau()==1) {
			return this.getPrixAchat()*Constantes.MAISON2;
		}else if(this.getNiveau()==2) {
			return this.getPrixAchat()*Constantes.MAISON3;
		}else {
			return this.getPrixAchat()*Constantes.IMMEUBLE;
		}
	}
	
	private static Propriete load(String toLoad) throws Exception {
		String[] datas = toLoad.split(";");
		if (datas.length == 4) {
			try {
				int id = Integer.valueOf(datas[0]);
				int price = Integer.valueOf(datas[3]);
				Propriete p = new Propriete(id, datas[2], price, null);
				return p;
			} catch (Exception e) {
				System.err.println(e.getMessage());
		}
			throw new Exception("Fichier CSV corrompu"); 
		}
		return null;
	}
	
	public static ArrayList<Propriete> importationProprietes() {
		ArrayList<Propriete> res = new ArrayList<Propriete>();
		try (BufferedReader br = new BufferedReader(new FileReader(Constantes.CSV_PROPRIETES))) {
			String line = br.readLine();
		    while (line != null) {
		    	Propriete p = Propriete.load(line);
		    	res.add(p);
		    	line = br.readLine();
		    }
		} catch (IOException e) {
			System.err.println(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}
	
	

}
