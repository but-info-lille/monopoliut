package jeu;

import java.util.Random;

public class Chance {
	
	public static void event(Joueur j, Plateau p) {
		Random random = new Random();
		int value = random.nextInt((10 - 1) + 1) + 1;
		System.out.println(value);
		if (value==1) {
			System.out.println(j.getNom()+": DS surprise! Allez à la case DS");
			j.departPrison(p);
		}else if (value==2) {
			System.out.println(j.getNom()+": Vous assistez à un amphi de réseau. Il n'était pas vraiment intéressant... -50$");
			j.setArgentPossede(j.getArgentPossede()-50);
		}else if (value==3) {
			System.out.println(j.getNom()+": Votre GIT vient de planter! Vous perdez une partie de votre code... -100$");
			j.setArgentPossede(j.getArgentPossede()-100);
		}else if (value==4) {
			System.out.println(j.getNom()+": Les gitans occupent le parking. Vous le traversez et ils vous racket. -50$");
			j.setArgentPossede(j.getArgentPossede()-50);
		}else if (value==5) {
			System.out.println(j.getNom()+": Beaufils vous complimente car votre code est magnifique. +100$ et +2000 d'estime (ce n'est pas une monnaie, dommage)");
			j.setArgentPossede(j.getArgentPossede()+100);
		}else if (value==6) {
			System.out.println(j.getNom()+": Vous arrivez en retard à un cours de Chlebowski et lui devez un café... -50$");
			j.setArgentPossede(j.getArgentPossede()-50);
		}else if (value==7) {
			System.out.println(j.getNom()+": Votre réveil à sonné trop tard et vous avez manqué un amphi de Graphe. +100$");
			j.setArgentPossede(j.getArgentPossede()+100);
		}else if (value==8) {
			System.out.println(j.getNom()+": Le RU est fermé aujourd'hui et vous ne pouvez pas manger. Vous économisez de l'argent! +50$");
			j.setArgentPossede(j.getArgentPossede()+50);
		}else if (value==9) {
			System.out.println(j.getNom()+": Baste vous aide pendant le CTP et vous finissez en avance! +50$");
			j.setArgentPossede(j.getArgentPossede()+50);
		}else if (value==10) {
			System.out.println("Vous vous êtes fait insulte par Carle et devez vous acheter une glace pour vous consoler... -50$");
			j.setArgentPossede(j.getArgentPossede()-50);
		}
	}
}
