package jeu;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProprieteTest {
	
	Propriete p1, p2, p3;
	Joueur j1, j2;
	
	@BeforeEach
	void setUp() {
		p1 = new Propriete(4, "Mathieu Street", 450, null);
		p2 = new Propriete(6, "Delecroix Street", 300, null);
		p3 = new Propriete(19, "Everaere Street", 300, null);
		j1 = new Joueur(1, "Tom");
		j2 = new Joueur(2, "Rose");
	}

	@Test
	void testToString() {
		assertEquals("Propriete [nom=Mathieu Street, prixAchat=450, proprio=null]", p1.toString());
		assertEquals("Propriete [nom=Delecroix Street, prixAchat=300, proprio=null]", p2.toString());
		assertEquals("Propriete [nom=Everaere Street, prixAchat=300, proprio=null]", p3.toString());
	}

	@Test
	void testEqualsObject() {
		Propriete p1Bis = p1;
		Propriete p2Bis = p2;
		Propriete p3Bis = new Propriete(19, "Everaere Street", 300, null);
		assertTrue(p1.equals(p1Bis));
		assertTrue(p2.equals(p2Bis));
		assertTrue(p3.equals(p3Bis));
		assertFalse(p1.equals(p2));
		assertFalse(p2.equals(p3));
		assertFalse(p3.equals(p1));
	}

	@Test
	void testGetNom() {
		assertEquals("Mathieu Street", p1.getNom());
		assertEquals("Delecroix Street", p2.getNom());
		assertEquals("Everaere Street", p3.getNom());
	}

	@Test
	void testGetPrixAchat() {
		assertEquals(450, p1.getPrixAchat());
		assertEquals(300, p2.getPrixAchat());
		assertEquals(300, p3.getPrixAchat());
	}

	@Test
	void testGetLoyer() {
		assertEquals(225, p1.getLoyer());
		assertEquals(150, p2.getLoyer());
		assertEquals(150, p3.getLoyer());
	}

	@Test
	void testGetNiveau() {
		assertEquals(0, p1.getNiveau());
		assertEquals(0, p2.getNiveau());
		assertEquals(0, p3.getNiveau());
	}

	@Test
	void testGetProprio() {
		assertEquals(null, p1.getProprio());
		assertEquals(null, p2.getProprio());
		assertEquals(null, p3.getProprio());
		p1.setProprio(j1);
		assertEquals(j1, p1.getProprio());
	}

	@Test
	void testGetPrixAmelioration() {
		assertEquals(200, p1.getPrixAmelioration());
		assertEquals(200, p2.getPrixAmelioration());
		assertEquals(200, p3.getPrixAmelioration());
	}

	@Test
	void testPrixAmelioration() {
		p1.setNiveau(2);
		assertEquals(675, p1.prixAmelioration());
		p2.setNiveau(1);
		assertEquals(360, p2.prixAmelioration());
	}

	@Test
	void testImportationProprietes() {
		List<Propriete> proprietes = Propriete.importationProprietes();
		assertEquals(21, proprietes.size());
		Propriete p4 = new Propriete(1, "Carle", 60, null);
		assertEquals(p4, proprietes.get(0));
	}

}
